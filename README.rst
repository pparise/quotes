######
Quotes
######


IMPORTANT UPDATE AS OF 2020-12-04
=================================

   The `Programming Quotes API`_ is now broken so the **quote-api.py** script
   will no longer work. As a workaround, a new script called **quote-json.py**
   was added. This script uses the full JSON source file found in the `Github repo`_
   backup folder. Since this file is over 180MB in size, and since it doesn't seem
   to be updated any more, it was found more pratical to simply download the file
   and use it locally instead of making an HTTP requests for the file on each call.

Intro
------

A series of command line tools to print random quotes to the console. The quotes
are decorated with a colored border.

Use **quote-api.py** to get a random programming quote from a web API. (**BROKEN**)

Use **quote-db.py** to get a random quote from an SQLite database.

Use **quote-json.py** to get a random quote from a JSON source file.

An SQLite quote database is provided with some quotes. Simply add your own.


* Free software: MIT license


Credits
--------

`Programming Quotes API`_ for open source projects provides a free API for
programming quotes in multiple languages. See the `Github repo`_ for details.

.. _`Programming Quotes API`: https://programming-quotes-api.herokuapp.com/
.. _`Github repo`: https://github.com/skolakoda/programming-quotes-api

