#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Random Quote.

Description:
    Print a random quote and it's author to the console using an
    SQLite database. The quote is decorated with a colored border.

Usage:
    quote -c blue | --color blue
    quote -h | --help
    quote -v | --version

Optional Arguments:
    -h --help           Show this help screen and exit.
    -v --version        Show version.
    -c --color          Border color. (default=yellow)
                        Allowed values are: red, green, orange, blue,
                        purple, cyan, lightgrey, darkgrey, lightred,
                        lightgreen, yellow, lightblue, pink, lightcyan

"""

__author__ = "pparise"
__version__ = "1.0.0"
__license__ = "MIT"

import os
import argparse
import sqlite3
import textwrap
import sys

# text formatting options and colors for the console
formatters = {
    'black': '\033[30m',
    'red': '\033[31m',
    'green': '\033[32m',
    'orange': '\033[33m',
    'blue': '\033[34m',
    'purple': '\033[35m',
    'cyan': '\033[36m',
    'lightgrey': '\033[37m',
    'darkgrey': '\033[90m',
    'lightred': '\033[91m',
    'lightgreen': '\033[92m',
    'yellow': '\033[93m',
    'lightblue': '\033[94m',
    'pink': '\033[95m',
    'lightcyan': '\033[96m',
    'reset': "\033[0m",
    'bold': '\033[1m',
    'italic': '\033[3m'
}


def main(args):
    """ Main entry point of the app """

    # if the namespace help attribute exists, then show help (the module doctring)
    if hasattr(args, 'help'):
        print(__doc__)
        sys.exit(1)

    # check if argument is a valid allowed color value
    if args.color not in formatters:
        print("Invalid color value supplied :-( Have you looked at the help?")
        sys.exit(1)

    # create a TextWrapper object instance and set the instance attributes
    wrapper = textwrap.TextWrapper()
    wrapper.initial_indent = " "
    wrapper.subsequent_indent = " "
    wrapper.width = 70

    # create a connection object
    database = os.path.join(os.path.dirname(__file__), 'quotes.sqlite3')
    con = sqlite3.connect(database)

    # create a cursor object
    c = con.cursor()

    # get a random quote
    c.execute("SELECT * FROM quotes ORDER BY RANDOM() LIMIT 1;")

    # fetch row
    r = c.fetchone()

    # get the quote and author
    quote, author = r[1], r[2]

    # wrap the quote (prepend a newline)
    quote = wrapper.fill(quote)

    # format the title (append a newline)
    author = " --" + author + "\n"

    # the full quote with and title
    fullquote = quote + author

    # print the full quote with a colored border
    print(bordered(fullquote, args.color))


def generate_argparser():
    """Create and return an argument parser object."""

    # create a ArgumentParser object instance and set the instance attributes
    parser = argparse.ArgumentParser(
        description='Random Quote Generator',
        epilog="That's all folks!",
        formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=36),
        add_help=False,
        allow_abbrev=False)

    # add argument for help
    parser.add_argument(
        '-h',
        '--help',
        action='store_true',
        dest='help',
        default=argparse.SUPPRESS)

    # add argument for version
    parser.add_argument(
        "-v",
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    # get a list of valid colors
    colors = list(formatters.keys())
    colors = colors[1:15]

    # optional color argument
    parser.add_argument(
        '-c',
        '--color',
        action='store',
        dest='color',
        default='yellow',
        type=None,
        help='Border color. Allowed values are: ' + ', '.join(colors),
        metavar='')

    return parser


def bordered(text, color):
    """ Print a border (or box) around a text string."""
    lines = text.splitlines()
    width = max(len(s) for s in lines) + 1
    res = ['{' + color + '}┌' + '─' * width + '┐{reset}'.format(**formatters)]
    for s in lines:
        res.append('{' + color + '}│{reset}' + (s + ' ' * width)[:width] + '{' + color + '}│{reset}')
    res.append('{' + color + '}└' + '─' * width + '┘{reset}'.format(**formatters))
    return '\n'.join(res).format(**formatters)


if __name__ == "__main__":
    """ This is executed when run from the command line."""
    parser = generate_argparser()
    args = parser.parse_args()
    main(args)
